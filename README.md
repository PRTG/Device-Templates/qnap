## PRTG Device Template for monitoring QNAP Expansion units connected to your NAS via SNMP with PRTG

This project is a custom device template that can be used to monitor expansion units and other metrics provided by QNAP's NAS units in PRTG using the auto-discovery for simplified sensor creation. It might work even on systems with no expansion units(available matrics will vary)

## Download
A zip file containing the template and necessary additional files can be downloaded from the repository using the link below:
- [Latest Version of the Template](https://gitlab.com/PRTG/Device-Templates/qnap/-/jobs/artifacts/master/download?job=PRTGDistZip)

## Installation Instructions
Please refer to INSTALL.md or refer to the following KB-Post:
- [How can I monitor QNAP Expansion Units](https://kb.paessler.com/en/topic/78319)